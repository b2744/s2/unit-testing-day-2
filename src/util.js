
//factorial function
const factorial = (n) => {

	if(n < 0) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	if(typeof(n) === "string") return false;

	return n * factorial(n-1);
}

//Check divisibility
const div_check = (n) => {
	if(n % 7 === 0 || n % 5 === 0) return 0;
	if(n % 7 === 0 && n % 5 === 0) return 0;

}

const names = {
	Eren : {
		"alias" : "AOT",
		"name" : " Eren Yeager",
		"age" : 28
	},
	Levi : {
		"alias" : "Ivel",
		"name" : "Levi Ackerman",
		"age" : 38
	}
}; 


//exporting
module.exports ={
	factorial : factorial, 
	div_check : div_check,
	names : names
}